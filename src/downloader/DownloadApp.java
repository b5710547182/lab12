package downloader;

import javax.swing.SwingUtilities;

/**
 * Launch the URL downloader application.
 */
public class DownloadApp {

	//http://download.kemonology.com/NDS/5800%20-%205899/5842%20-%20Harvest%20Moon%20-%20Grand%20Bazaar%20(E)(SUXXORS).zip

	/**
	 * Main method to start the user interface.
	 */
	public static void main(String[] args) {
		DownloaderUI ui = new DownloaderUI();
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				ui.run();
			}
		});;

	}
}
